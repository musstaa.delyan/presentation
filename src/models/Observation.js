const mongoose = require("mongoose");

const { Schema } = mongoose;

const ObservationSchema = new Schema({
  full_name: { type: String },
  email: { type: String },
  title: { type: String },
  description: { type: String },
  date: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Observation", ObservationSchema);
