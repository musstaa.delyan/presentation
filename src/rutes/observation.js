const express = require("express");
const Observation = require("./../models/Observation");
const router = express.Router();

router.get("/observation", (req, res) => {
  res.render("observation/add-observation");
});

router.post("/observation/new-observation", async (req, res) => {
  const { full_name, email, title, description } = req.body;
  const errors = [];
  if (!full_name) {
    errors.push({ text: "You Have to Write Your Full Name Please" });
  }
  if (!email) {
    errors.push({ text: "You Have to Write Your Email Please" });
  }
  if (!title) {
    errors.push({ text: "You Have to Write a Title Please" });
  }
  if (!description) {
    errors.push({ text: "You Have to Write a Description Please" });
  }
  if (errors.length > 0) {
    res.render("observation/add-observation", {
      errors,
      full_name,
      email,
      title,
      description,
    });
  } else {
    const newObservation = new Observation({
      full_name,
      email,
      title,
      description,
    });
    await newObservation.save();
    res.redirect("/me");
  }
});

router.get("/observation/all", async (req, res) => {
  const observation = await Observation.find({}).lean();
  res.render("observation/all-observation", { observation });
});
module.exports = router;
