const express = require("express");

const router = express.Router();

router.get("/me", (req, res) => {
  res.render("presentation");
});

router.get("/me/about", (req, res) => {
  res.render("me/about");
});
router.get("/me/objective", (req, res) => {
  res.render("me/objective");
});
router.get("/me/contact", (req, res) => {
  res.render("me/contact");
});

module.exports = router;
