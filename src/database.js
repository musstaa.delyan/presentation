const mongoose = require("mongoose");

mongoose
  .connect("mongodb://localhost/observation-db-app", { useNewUrlParser: true })
  .then(db => {
    console.log("DB is connected correctly 👍");
  })
  .catch(err => {
    console.error(err);
  });
