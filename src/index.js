const express = require("express");
const path = require("path");
const dotenv = require("dotenv").config();
const exphbs = require("express-handlebars");
const port = process.env.PORT || 3000;

//Initialization
const app = express();
require("./database");

//Setting
app.set("port", port);
app.set("views", path.join(__dirname, "views"));
app.engine(
  "hbs",
  exphbs({
    defaultLayout: "main",
    layoutsDir: path.join(app.get("views"), "layouts"),
    partialsDir: path.join(app.get("views"), "partials"),
    extname: ".hbs",
  })
);
app.set("view engine", "hbs");

//Middleware
app.use(express.urlencoded({ extended: false }));

//Routes
const index = require("./rutes/index");
const about = require("./rutes/about");
const observation = require("./rutes/observation");

app.use(about);
app.use(index);
app.use(observation);

//Static Files
app.use(express.static(path.join(__dirname, "public")));

//Server on
app.listen(app.get("port"), () => {
  console.log("Server on port", app.get("port"));
});
