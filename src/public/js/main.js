const about = _ => {
  location.href = "http://localhost:5000/me/about";
};

const objective = _ => {
  location.href = "http://localhost:5000/me/objective";
};
const contact = _ => {
  location.href = "http://localhost:5000/me/contact";
};
const observation = _ => {
  location.href = "http://localhost:5000/observation";
};
document.getElementById("about-container").addEventListener("click", about);
document
  .getElementById("objective-container")
  .addEventListener("click", objective);
document.getElementById("contact-container").addEventListener("click", contact);
document
  .getElementById("observation-container")
  .addEventListener("click", observation);

const mouseImage = event => {
  event.target.style.cursor = "pointer";
};
document
  .querySelector("#about-container")
  .addEventListener("mouseover", mouseImage);
document
  .querySelector("#objective-container")
  .addEventListener("mouseover", mouseImage);
document
  .querySelector("#observation-container")
  .addEventListener("mouseover", mouseImage);
document
  .querySelector("#contact-container")
  .addEventListener("mouseover", mouseImage);
